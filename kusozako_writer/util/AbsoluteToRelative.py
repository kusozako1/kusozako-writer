# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio


def _get_directory(source_path, depth):
    gfile = Gio.File.new_for_path(source_path)
    while depth > 0:
        depth -= 1
        gfile = gfile.get_parent()
    return gfile.get_path()


def _get_depth(path, source_path):
    depth = 1
    while True:
        directory = _get_directory(source_path, depth)
        if path.startswith(directory):
            return depth, directory
        depth += 1


def get_path(path, source_path):
    depth, parent = _get_depth(path, source_path)
    parent_gfile = Gio.File.new_for_path(parent)
    relative_base = parent_gfile.get_relative_path(
        Gio.File.new_for_path(path)
        )
    relative_parent = "../"*(depth-1)
    return relative_parent+relative_base
