# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.sub_page.SubPage import AlfaSubPage
from kusozako_writer.const import ExtraMenuPages
from .ViewConfigButton import DeltaViewConfigButton
from .StyleSchemeButton import DeltaStyleSchemeButton
from .SelectFontButton import DeltaSelectFontButton


class DeltaWriterPage(AlfaSubPage):

    PAGE_NAME = ExtraMenuPages.WRITER

    def _on_initialize(self):
        DeltaViewConfigButton(self)
        DeltaStyleSchemeButton(self)
        DeltaSelectFontButton(self)
