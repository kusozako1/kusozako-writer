# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.overlay_item.button.Button import AlfaButton
from kusozako1.util import SafePath
from kusozako1.file_dialog_2.FileDialog import DeltaFileDialog
from kusozako_writer.const import EditorSignals


class DeltaPdf(AlfaButton):

    LABEL = _("To PDF")

    def _delta_call_dialog_response(self, gfile):
        if gfile.query_exists():
            print("file exists")
            return
        user_data = EditorSignals.EXPORT_TO_PDF, gfile
        self._raise("delta > editor signal", user_data)

    def _on_clicked(self, button):
        query = "directory", "export"
        directory = self._enquiry("delta > settings", query)
        gfile = Gio.File.new_for_path(directory)
        filename = SafePath.get_safe_name(gfile, "foobar.pdf")
        DeltaFileDialog.save_file(
            self,
            title=_("Export file to..."),
            mime_types=["application/pdf"],
            default_filename=filename,
            default_directory=gfile,
            )
