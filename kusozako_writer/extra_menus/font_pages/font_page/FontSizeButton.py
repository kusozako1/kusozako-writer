# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton
from kusozako1.const import MainWindowSignals as Signals
from kusozako_writer.const import ExtraMenuPages


class DeltaFontSizeButton(AlfaButton):

    LABEL = _("Font Size")
    END_ICON = "go-next-symbolic"

    def _on_clicked(self, button):
        user_data = Signals.SHOW_EXTRA_PRIMARY_MENU, ExtraMenuPages.FONT_SIZE
        self._raise("delta > main window signal", user_data)
