# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.sub_page.SubPage import AlfaSubPage
from kusozako_writer.const import ExtraMenuPages
from .FontFamilyButton import DeltaFontFamilyButton
from .FontSizeButton import DeltaFontSizeButton
from .FontWeightButton import DeltaFontWeightButton


class DeltaFontPage(AlfaSubPage):

    PAGE_NAME = ExtraMenuPages.FONT
    BACK_TO = ExtraMenuPages.WRITER

    def _on_initialize(self):
        DeltaFontFamilyButton(self)
        DeltaFontSizeButton(self)
        DeltaFontWeightButton(self)
