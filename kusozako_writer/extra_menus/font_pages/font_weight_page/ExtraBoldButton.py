# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .FontWeightButton import AlfaFontWeightButton


class DeltaExtraBoldButton(AlfaFontWeightButton):

    LABEL = _("Extra Bold (Ultra Bold)")
    MATCH_VALUE = 800
