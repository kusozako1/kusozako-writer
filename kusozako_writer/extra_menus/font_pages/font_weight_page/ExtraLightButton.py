# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .FontWeightButton import AlfaFontWeightButton


class DeltaExtraLightButton(AlfaFontWeightButton):

    LABEL = _("Extra Light (Ultra Light)")
    MATCH_VALUE = 200
