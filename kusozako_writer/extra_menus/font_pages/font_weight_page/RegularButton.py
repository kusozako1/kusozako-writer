# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .FontWeightButton import AlfaFontWeightButton


class DeltaRegularButton(AlfaFontWeightButton):

    LABEL = _("Regular")
    MATCH_VALUE = 400
