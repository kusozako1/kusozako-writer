# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.sub_page.SubPage import AlfaSubPage
from kusozako_writer.const import ExtraMenuPages
from .ThinButton import DeltaThinButton
from .ExtraLightButton import DeltaExtraLightButton
from .LightButton import DeltaLightButton
from .RegularButton import DeltaRegularButton
from .MediumButton import DeltaMediumButton
from .SemiBoldButton import DeltaSemiBoldButton
from .BoldButton import DeltaBoldButton
from .ExtraBoldButton import DeltaExtraBoldButton
from .BlackButton import DeltaBlackButton


class DeltaFontWeightPage(AlfaSubPage):

    PAGE_NAME = ExtraMenuPages.FONT_WEIGHT
    BACK_TO = ExtraMenuPages.FONT

    def _on_initialize(self):
        DeltaThinButton(self)
        DeltaExtraLightButton(self)
        DeltaLightButton(self)
        DeltaRegularButton(self)
        DeltaMediumButton(self)
        DeltaSemiBoldButton(self)
        DeltaBoldButton(self)
        DeltaExtraBoldButton(self)
        DeltaBlackButton(self)
