# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .FontWeightButton import AlfaFontWeightButton


class DeltaSemiBoldButton(AlfaFontWeightButton):

    LABEL = _("Semi Bold (Demi Bold)")
    MATCH_VALUE = 600
