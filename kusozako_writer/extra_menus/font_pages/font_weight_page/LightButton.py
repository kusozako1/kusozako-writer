# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .FontWeightButton import AlfaFontWeightButton


class DeltaLightButton(AlfaFontWeightButton):

    LABEL = _("Light")
    MATCH_VALUE = 300
