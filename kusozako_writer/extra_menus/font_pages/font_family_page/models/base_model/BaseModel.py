# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .Builder import DeltaBuilder


class DeltaBaseModel(Gio.ListStore, DeltaEntity):

    def _exists(self, bravo_name):
        for alfa in self:
            if alfa.get_string() == bravo_name:
                return True
        return False

    def _compare_data_func(self, alfa, bravo, user_data=None):
        return 1 if alfa.get_string() >= bravo.get_string() else -1

    def _delta_call_data_found(self, data):
        family_name = data.split(":")[1].split(",")[0]
        if self._exists(family_name):
            return
        font_entity = Gtk.StringObject.new(family_name)
        self.insert_sorted(font_entity, self._compare_data_func)

    def __init__(self, parent):
        self._parent = parent
        Gio.ListStore.__init__(self, item_type=Gtk.StringObject)
        DeltaBuilder(self)
