# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.sub_page.SubPage import AlfaSubPage
from kusozako_writer.const import ExtraMenuPages
from .models.Models import DeltaModels
from .list_view.ListView import DeltaListView


class DeltaFontFamilyPage(AlfaSubPage):

    PAGE_NAME = ExtraMenuPages.FONT_FAMILY
    BACK_TO = ExtraMenuPages.FONT

    def _delta_info_model(self):
        return self._models.get_selection_model()

    def _on_initialize(self):
        self._models = DeltaModels(self)
        DeltaListView(self)
