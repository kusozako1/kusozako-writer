# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.sub_page.SubPage import AlfaSubPage
from kusozako_writer.const import ExtraMenuPages
from .ExtraExtraSmallButton import DeltaExtraExtraSmallButton
from .ExtraSmallButton import DeltaExtraSmallButton
from .SmallButton import DeltaSmallButton
from .MediumButton import DeltaMediumButton
from .LargeButton import DeltaLargeButton
from .ExtraLargeButton import DeltaExtraLargeButton
from .ExtraExtraLargeButton import DeltaExtraExtraLargeButton


class DeltaFontSizePage(AlfaSubPage):

    PAGE_NAME = ExtraMenuPages.FONT_SIZE
    BACK_TO = ExtraMenuPages.FONT

    def _on_initialize(self):
        DeltaExtraExtraSmallButton(self)
        DeltaExtraSmallButton(self)
        DeltaSmallButton(self)
        DeltaMediumButton(self)
        DeltaLargeButton(self)
        DeltaExtraLargeButton(self)
        DeltaExtraExtraLargeButton(self)
