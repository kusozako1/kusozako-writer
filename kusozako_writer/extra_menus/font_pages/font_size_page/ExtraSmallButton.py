# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .FontSizeButton import AlfaFontSizeButton


class DeltaExtraSmallButton(AlfaFontSizeButton):

    LABEL = _("Extra Small")
    MATCH_VALUE = "x-small"
