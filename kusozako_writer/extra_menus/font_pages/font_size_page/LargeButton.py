# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .FontSizeButton import AlfaFontSizeButton


class DeltaLargeButton(AlfaFontSizeButton):

    LABEL = _("Large")
    MATCH_VALUE = "large"
