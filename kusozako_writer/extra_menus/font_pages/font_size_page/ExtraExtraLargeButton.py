# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .FontSizeButton import AlfaFontSizeButton


class DeltaExtraExtraLargeButton(AlfaFontSizeButton):

    LABEL = _("Extra Extra Large")
    MATCH_VALUE = "xx-large"
