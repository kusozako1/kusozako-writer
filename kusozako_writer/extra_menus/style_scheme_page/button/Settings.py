# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity


class DeltaSettings(DeltaEntity):

    def _refresh(self, current_id):
        own_id = self._enquiry("delta > id")
        if own_id == current_id:
            name = "radio-checked-symbolic"
        else:
            name = "radio-symbolic"
        self._raise("delta > icon changed", name)

    def receive_transmission(self, user_data):
        group, key, id_ = user_data
        if group == "view" and key == "style_scheme":
            self._refresh(id_)

    def __init__(self, parent):
        self._parent = parent
        query = "view", "style_scheme", "Adwaita"
        id_ = self._enquiry("delta > settings", query)
        self._refresh(id_)
        self._raise("delta > register settings object", self)
