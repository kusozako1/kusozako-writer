# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .Settings import DeltaSettings


class DeltaButton(Gtk.Button, DeltaEntity):

    @classmethod
    def new(cls, parent, id_):
        instance = cls(parent)
        instance.construct(id_)

    def _on_clicked(self, button):
        user_data = "view", "style_scheme", self._id
        self._raise("delta > settings", user_data)

    def _delta_info_id(self):
        return self._id

    def _delta_call_icon_changed(self, icon_name):
        self._image.set_from_icon_name(icon_name)

    def construct(self, id_):
        self._id = id_
        self._label.set_label(self._id)
        self.connect("clicked", self._on_clicked)
        DeltaSettings(self)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self)
        self.add_css_class("kusozako-primary-widget")
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=4)
        self.set_child(box)
        self._image = Gtk.Image()
        box.append(self._image)
        self._label = Gtk.Label(xalign=0)
        box.append(self._label)
        self._raise("delta > add to container", self)
