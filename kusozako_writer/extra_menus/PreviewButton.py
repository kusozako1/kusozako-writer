# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton
from kusozako1.const import MainWindowSignals
from kusozako_writer.const import EditorSignals


class DeltaPreviewButton(AlfaButton):

    LABEL = _("Preview")

    def _close(self):
        user_data = MainWindowSignals.CLOSE_OVERLAY, None
        self._raise("delta > main window signal", user_data)

    def _on_clicked(self, button):
        self._close()
        user_data = EditorSignals.PREVIEW, None
        self._raise("delta > editor signal", user_data)

    def _set_to_container(self):
        user_data = MainWindowSignals.ADD_EXTRA_MENU, self
        self._raise("delta > main window signal", user_data)
