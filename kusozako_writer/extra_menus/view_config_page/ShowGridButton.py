# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.CheckButton import AlfaCheckButton


class DeltaShowGridButton(AlfaCheckButton):

    LABEL = _("Show Grid")
    GROUP = "view"
    KEY = "show_grid"
    MATCH_VALUE = True
