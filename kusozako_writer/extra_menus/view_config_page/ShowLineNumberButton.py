# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.CheckButton import AlfaCheckButton


class DeltaShowLineNumberButton(AlfaCheckButton):

    LABEL = _("Show Line Number")
    GROUP = "view"
    KEY = "show_line_number"
    MATCH_VALUE = True
