# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.CheckButton import AlfaCheckButton


class DeltaInsertSpaceInsteadOfTabButton(AlfaCheckButton):

    LABEL = _("Insert Spaces Instead of Tabs")
    GROUP = "view"
    KEY = "insert_spaces_instead_of_tabs"
    MATCH_VALUE = True
