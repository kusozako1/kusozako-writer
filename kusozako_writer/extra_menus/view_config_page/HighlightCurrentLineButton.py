# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.CheckButton import AlfaCheckButton


class DeltaHighlightCurrentLineButton(AlfaCheckButton):

    LABEL = _("Highlight Current Line")
    GROUP = "view"
    KEY = "highlight_current_line"
    MATCH_VALUE = True
