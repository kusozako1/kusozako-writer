# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.main_loop.MainLoop import AlfaMainLoop
from kusozako1.Transmitter import FoxtrotTransmitter
from . import APPLICATION_DATA
from .DefaultSettings import DeltaDefaultSettings
from .data_layer.DataLayer import DeltaDataLayer
from .extra_menus.ExtraMenus import EchoExtraMenus
from .widget_layer.WidgetLayer import DeltaWidgetLayer


class DeltaMainLoop(AlfaMainLoop):

    def _delta_call_register_editor_object(self, object_):
        self._transmitter.register_listener(object_)

    def _delta_call_editor_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def _delta_call_loopback_editor_data_layer_ready(self, parent):
        DeltaWidgetLayer(parent)
        EchoExtraMenus(parent)

    def _delta_call_loopback_application_window_ready(self, parent):
        self._transmitter = FoxtrotTransmitter()
        DeltaDefaultSettings(parent)
        DeltaDataLayer(parent)

    def _delta_info_resource_directory(self):
        names = [GLib.path_get_dirname(__file__), "resource"]
        return GLib.build_filenamev(names)

    def _delta_info_data(self, key):
        return APPLICATION_DATA.get(key, None)
