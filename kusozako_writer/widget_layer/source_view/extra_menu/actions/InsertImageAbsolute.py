# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.FileDialog import DeltaFileDialog
from kusozako_writer.const import EditorSignals


class DeltaInserImageAbsolute(DeltaEntity):

    def _delta_call_dialog_response(self, gfile):
        markdown = "![]({})".format(gfile.get_path())
        user_data = EditorSignals.INSERT_AT_CURSOR, markdown
        self._raise("delta > editor signal", user_data)

    def _activate(self, view, action_name, user_data=None):
        DeltaFileDialog.select_file(
            self,
            title=_("Select Image File"),
            mime_types=["image/jpeg", "image/jpg", "image/png", "image/webp"],
            )

    def __init__(self, parent):
        self._parent = parent
        view = self._enquiry("delta > view")
        view.install_action("edit.insert-image-absolute", None, self._activate)
