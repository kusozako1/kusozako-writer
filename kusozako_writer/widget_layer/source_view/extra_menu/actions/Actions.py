# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .InsertImageAbsolute import DeltaInserImageAbsolute
from .InsertImageRelative import DeltaInserImageRelative


class EchoActions:

    def __init__(self, parent):
        DeltaInserImageAbsolute(parent)
        DeltaInserImageRelative(parent)
