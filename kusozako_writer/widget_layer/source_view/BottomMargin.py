# (c) copyright 2024-2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity

BOTTOM_MARGIN = 120


class DeltaBottomMargin(DeltaEntity):

    def _try_move_curosor(self, buffer, vadjustment):
        line_count = buffer.get_line_count()
        iter_ = buffer.get_iter_at_offset(buffer.props.cursor_position)
        line = iter_.get_line()
        if 5 >= line_count - line:
            vadjustment.set_value(GLib.MAXINT32)

    def _on_move_cursor(self, view, step, count, selection, vadjustment):
        buffer = view.get_buffer()
        self._try_move_curosor(buffer, vadjustment)

    def _on_changed(self, buffer, source_view, vadjustment):
        self._try_move_curosor(buffer, vadjustment)

    def __init__(self, parent):
        self._parent = parent
        view = self._enquiry("delta > view")
        scrolled_window = view.get_parent()
        vadjustment = scrolled_window.get_vadjustment()
        view.connect_after("move-cursor", self._on_move_cursor, vadjustment)
        buffer = view.get_buffer()
        buffer.connect_after("changed", self._on_changed, view, vadjustment)
