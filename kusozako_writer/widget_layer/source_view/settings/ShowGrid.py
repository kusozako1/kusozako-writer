# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GtkSource
from .SettingsEntity import AlfaSettingsEntity


class DeltaShowGrid(AlfaSettingsEntity):

    KEY = "show_grid"
    DEFAULT_VALUE = True
    PROPERTY_NAME = "background-pattern"

    def _refresh(self, show_grid):
        if show_grid:
            background_pattern = GtkSource.BackgroundPatternType.GRID
        else:
            background_pattern = GtkSource.BackgroundPatternType.NONE
        view = self._enquiry("delta > view")
        view.set_property(self.PROPERTY_NAME, background_pattern)
