# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .SettingsEntity import AlfaSettingsEntity


class DeltaHighlightCurrentLine(AlfaSettingsEntity):

    KEY = "highlight_current_line"
    DEFAULT_VALUE = False
    PROPERTY_NAME = "highlight-current-line"
