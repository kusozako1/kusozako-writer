# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .SettingsEntity import AlfaSettingsEntity


class DeltaTabWidth(AlfaSettingsEntity):

    KEY = "tab_width"
    DEFAULT_VALUE = 4
    PROPERTY_NAME = "tab-width"
