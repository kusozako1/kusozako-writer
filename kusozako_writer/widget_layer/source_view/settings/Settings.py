# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .ShowGrid import DeltaShowGrid
from .ShowLineNumber import DeltaShowLineNumber
from .HighlightCurrentLine import DeltaHighlightCurrentLine
from .InsertSpacesInsteadOfTabs import DeltaInsertSpacesInsteadOfTabs
from .TabWidth import DeltaTabWidth


class EchoSettings:

    def __init__(self, parent):
        DeltaShowGrid(parent)
        DeltaShowLineNumber(parent)
        DeltaHighlightCurrentLine(parent)
        DeltaInsertSpacesInsteadOfTabs(parent)
        DeltaTabWidth(parent)
