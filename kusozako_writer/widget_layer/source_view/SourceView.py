# (c) copyright 2024-2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GtkSource
from kusozako1.Entity import DeltaEntity
from .BottomMargin import DeltaBottomMargin
from .settings.Settings import EchoSettings
from .extra_menu.ExtraMenu import EchoExtraMenu
from .watchers.Watchers import EchoWatchers

BOTTOM_MARGIN = 120


class DeltaSourceView(GtkSource.View, DeltaEntity):

    def _delta_info_view(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow(hexpand=True, vexpand=True)
        GtkSource.View.__init__(
            self,
            buffer=self._enquiry("delta > buffer"),
            bottom_margin=BOTTOM_MARGIN,
            )
        scrolled_window.set_child(self)
        DeltaBottomMargin(self)
        EchoExtraMenu(self)
        EchoSettings(self)
        EchoWatchers(self)
        self._raise("delta > add to container", scrolled_window)
