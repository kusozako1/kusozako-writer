# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .source_view.SourceView import DeltaSourceView
from .web_view.WebView import DeltaWebView

DEFAULT_POSITION = 400


class DeltaWidgetLayer(Gtk.Paned, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        if self.get_start_child() is None:
            self.set_start_child(widget)
        else:
            self.set_end_child(widget)
            query = "paned", "position", DEFAULT_POSITION
            width = self._enquiry("delta > settings", query)
            self.set_position(width)

    def _on_notify_position(self, widget, param_spec):
        settings = "paned", "position", self.get_position()
        self._raise("delta > settings", settings)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Paned.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            wide_handle=True,
            resize_start_child=False,
            )
        self.connect("notify::position", self._on_notify_position)
        DeltaSourceView(self)
        DeltaWebView(self)
        self._raise("delta > add to container", self)
