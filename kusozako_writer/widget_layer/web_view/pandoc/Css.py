# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity


class DeltaCss(DeltaEntity):

    def get_current_uri(self):
        return self._css_uri

    def __init__(self, parent):
        self._parent = parent
        resource_directory = self._enquiry("delta > resource directory")
        names = [resource_directory, "css", "ocean.css"]
        css_path = GLib.build_filenamev(names)
        css_gfile = Gio.File.new_for_path(css_path)
        self._css_uri = css_gfile.get_uri()
