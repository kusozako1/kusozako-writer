# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_writer.const import EditorSignals
from .Css import DeltaCss
from .SubprocessLauncher import DeltaSubprocessLauncher


class DeltaPandoc(DeltaEntity):

    def _on_finished(self, subprocess, task, line_number):
        successful, stdout, stderr = subprocess.communicate_utf8_finish(task)
        if successful:
            self._raise("delta > html5 created", stdout)
            user_data = EditorSignals.WEB_VIEW_SET_POSITION, line_number
            self._raise("delta > editor signal", user_data)
        else:
            self._raise("delta > html5 created", "something goes wrong")

    def _on_signal_received(self, markdown, line_number):
        command_line = [
            "pandoc",
            "-f", "markdown",
            "-t", "html5",
            "--css", self._css.get_current_uri(),
            "--standalone",
            "--embed-resources",
            "--toc",
            ]
        subprocess = self._subprocess_launcher.get_subprocess(command_line)
        subprocess.communicate_utf8_async(
            markdown,
            None,
            self._on_finished,
            line_number
            )

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != EditorSignals.WEB_VIEW_REQUSET_REFRESH:
            return
        markdown, line_number = param
        self._on_signal_received(markdown, line_number)

    def __init__(self, parent):
        self._parent = parent
        self._css = DeltaCss(self)
        self._subprocess_launcher = DeltaSubprocessLauncher(self)
        self._raise("delta > register editor object", self)
