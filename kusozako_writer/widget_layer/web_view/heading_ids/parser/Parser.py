# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_writer.const import EditorSignals
from .CodeBlockChecker import FoxtrotCodeBlockChecker
from .SafeId import DeltaSafeId


class DeltaParser(DeltaEntity):

    def _parse_line(self, line_index, line):
        unsafe_id = line.split(" ", 1)[1]
        safe_id = self._safe_id.get_safe_id(unsafe_id)
        user_data = line_index, safe_id
        self._raise("delta > heading found", user_data)

    def _on_signal_received(self, markdown):
        self._raise("delta > clear all")
        lines = markdown.split("\n")
        self._code_block_checker.reset()
        for line_index in range(0, len(lines)):
            line = lines[line_index]
            if not self._code_block_checker.is_heading(line):
                continue
            self._parse_line(line_index, line)

    def receive_transmission(self, user_data):
        signal, markdown = user_data
        if signal != EditorSignals.BUFFER_CHANGED:
            return
        self._on_signal_received(markdown)

    def __init__(self, parent):
        self._parent = parent
        self._code_block_checker = FoxtrotCodeBlockChecker()
        self._safe_id = DeltaSafeId(self)
        self._raise("delta > register editor object", self)
