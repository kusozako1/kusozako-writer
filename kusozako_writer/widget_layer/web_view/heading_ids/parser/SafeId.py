# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity


class DeltaSafeId(DeltaEntity):

    def _id_exists(self, unsafe_id):
        for item in self._enquiry("delta > model"):
            if unsafe_id == item.get_id():
                return True
        return False

    def _get_indexed_id(self, unsafe_id):
        index = 0
        while True:
            index += 1
            safe_id = "{}+{}".format(unsafe_id, index)
            if not self._id_exists(safe_id):
                return safe_id

    def get_safe_id(self, source_id):
        unsafe_id = GLib.utf8_strdown(source_id, -1)
        unsafe_id = unsafe_id.strip()
        unsafe_id = unsafe_id.replace("/", "")
        unsafe_id = unsafe_id.replace("~", "")
        split_id = unsafe_id.split(" ")
        join_id = "-".join(split_id)
        if not self._id_exists(join_id):
            return join_id
        return self._get_indexed_id(join_id)

    def __init__(self, parent):
        self._parent = parent
