# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.Entity import DeltaEntity
from .HeadingItem import FoxtrotHeadingItem
from .parser.Parser import DeltaParser
from .CursorMoved import DeltaCursorMoved


class DeltaHeadingIds(Gio.ListStore, DeltaEntity):

    def _sort_func(self, alfa, bravo, user_data=None):
        return bravo.get_line_index() > alfa.get_line_index()

    def _delta_call_heading_found(self, user_data):
        line_index, id_ = user_data
        item = FoxtrotHeadingItem(line_index, id_)
        self.insert_sorted(item, self._sort_func)

    def _delta_call_clear_all(self):
        self.remove_all()

    def _delta_info_model(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        Gio.ListStore.__init__(self, item_type=FoxtrotHeadingItem)
        DeltaParser(self)
        DeltaCursorMoved(self)
