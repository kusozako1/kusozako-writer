# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_writer.const import EditorSignals


class DeltaCursorMoved(DeltaEntity):

    def _move_to(self, line):
        for item in self._enquiry("delta > model"):
            if line >= item.get_line_index():
                self._raise("delta > move to id", item.get_id())
                return

    def receive_transmission(self, user_data):
        signal, line = user_data
        if signal != EditorSignals.WEB_VIEW_SET_POSITION:
            return
        self._move_to(line)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register editor object", self)
