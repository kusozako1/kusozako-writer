# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.const import UserSpecialDirectories

SETTINGS = (
    ("directory", "export", UserSpecialDirectories.DOCUMENTS),
    )


class DeltaDefaultSettings(DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        for setting in SETTINGS:
            self._raise("delta > settings", setting)
