# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GtkSource
from kusozako1.Entity import DeltaEntity
from .LanguageManager import DeltaLanguageManager
from .StyleSchemeManager import DeltaStyleSchemeManager
from .watchers.Watchers import EchoWatchers
from .observers.Observers import EchoObservers


class DeltaBuffer(GtkSource.Buffer, DeltaEntity):

    def _delta_info_buffer(self):
        return self

    def _delta_call_language_changed(self, language):
        self.set_language(language)

    def _delta_call_style_scheme_changed(self, style_scheme):
        self.set_style_scheme(style_scheme)

    def __init__(self, parent):
        self._parent = parent
        GtkSource.Buffer.__init__(self)
        DeltaLanguageManager(self)
        DeltaStyleSchemeManager(self)
        EchoWatchers(self)
        EchoObservers(self)
        self._raise("delta > loopback buffer ready", self)
