# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako_writer.const import EditorSignals


class DeltaChanged(DeltaEntity):

    def _timeout(self, buffer, index):
        if self._index != index:
            return GLib.SOURCE_REMOVE
        text = buffer.get_text(
            buffer.get_start_iter(),
            buffer.get_end_iter(),
            False,
            )
        user_data = EditorSignals.BUFFER_CHANGED, text
        self._raise("delta > editor signal", user_data)
        return GLib.SOURCE_REMOVE

    def _on_changed(self, buffer):
        self._index += 1
        index = self._index
        GLib.timeout_add(1000, self._timeout, buffer, index)

    def __init__(self, parent):
        self._parent = parent
        self._index = 0
        buffer_ = self._enquiry("delta > buffer")
        buffer_.connect("changed", self._on_changed)
