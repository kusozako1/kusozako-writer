# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from .buffer.Buffer import DeltaBuffer
from .file.File import DeltaFile


class DeltaDataLayer(DeltaEntity):

    def _delta_call_loopback_buffer_ready(self, parent):
        DeltaFile(parent)

    def __init__(self, parent):
        self._parent = parent
        DeltaBuffer(self)
