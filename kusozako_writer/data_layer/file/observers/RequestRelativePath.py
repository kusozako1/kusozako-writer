# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_writer.const import EditorSignals
from kusozako_writer.util import AbsoluteToRelative


class DeltaRequestRelativePath(DeltaEntity):

    def _on_received(self, image_gfile, location_gfile):
        image_path = image_gfile.get_path()
        location_path = location_gfile.get_path()
        relative_path = AbsoluteToRelative.get_path(image_path, location_path)
        markdown = "![]({})".format(relative_path)
        user_data = EditorSignals.INSERT_AT_CURSOR, markdown
        self._raise("delta > editor signal", user_data)

    def receive_transmission(self, user_data):
        signal, image_gfile = user_data
        if signal != EditorSignals.REQUEST_RELATIVE_PATH:
            return
        file_ = self._enquiry("delta > file")
        location_gfile = file_.get_location()
        if location_gfile is None:
            print("file not saved")
            return
        self._on_received(image_gfile, location_gfile)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register editor object", self)
