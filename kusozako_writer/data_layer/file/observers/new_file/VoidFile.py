# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_writer.const import EditorSignals


class DeltaVoidFile(DeltaEntity):

    def set_void(self):
        source_file = self._enquiry("delta > file")
        source_file.set_location(None)
        buffer_ = self._enquiry("delta > buffer")
        buffer_.set_text("")
        user_data = EditorSignals.FILE_SAVED, None
        self._raise("delta > editor signal", user_data)

    def __init__(self, parent):
        self._parent = parent
