# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.Entity import DeltaEntity


class AlfaExporter(DeltaEntity):

    __signal__ = "define signal here."

    def _on_finished(self, subprocess, task, destination):
        successful, _, stderr = subprocess.communicate_utf8_finish(task)
        if successful:
            print("file successfully exported to {}".format(destination))
        else:
            print("error ?", stderr)

    def _get_subprocess(self, cwd, command_line):
        subprocess_launcher = Gio.SubprocessLauncher.new(
            Gio.SubprocessFlags.STDIN_PIPE
            )
        subprocess_launcher.set_cwd(cwd)
        return subprocess_launcher.spawnv(command_line)

    def _on_received(self, markdown, gfile):
        raise NotImplementedError()

    def receive_transmission(self, user_data):
        signal, gfile = user_data
        if signal != self.__signal__:
            return
        buffer = self._enquiry("delta > buffer")
        markdown = buffer.get_text(
            buffer.get_start_iter(),
            buffer.get_end_iter(),
            False,
            )
        self._on_received(markdown, gfile)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register editor object", self)
