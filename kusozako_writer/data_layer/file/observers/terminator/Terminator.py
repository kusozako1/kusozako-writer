# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.const import MainWindowSignals
from kusozako_writer.alfa.file_handler.FileHandler import AlfaFileHandler
from .Closable import DeltaClosable


class DeltaTerminator(AlfaFileHandler):

    def _post_sequence(self):
        self._closable.force_close()

    def _on_initialize(self):
        self._closable = DeltaClosable(self)
        user_data = MainWindowSignals.REPLACE_TERMINATOR, self
        self._raise("delta > main window signal", user_data)

    def is_closable(self):
        return self._closable.request()
