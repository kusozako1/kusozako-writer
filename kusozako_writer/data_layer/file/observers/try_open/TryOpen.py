# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_writer.alfa.file_handler.FileHandler import AlfaFileHandler
from .SignalReceiver import DeltaSignalReceiver
from .OpenFile import DeltaOpenFile


class DeltaTryOpen(AlfaFileHandler):

    def _post_sequence(self):
        self._open_file.open_file()

    def _on_initialize(self):
        self._open_file = DeltaOpenFile(self)
        DeltaSignalReceiver(self)
