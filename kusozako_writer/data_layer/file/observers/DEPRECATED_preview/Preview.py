# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from .pandoc.Pandoc import DeltaPandoc
from .post_conversion.PostConversion import DeltaPostConversion
from .SignalReceiver import DeltaSignalReceiver


class DeltaPreview(DeltaEntity):

    def _delta_call_create_preview(self, source_path):
        self._pandoc.create_preview(source_path)

    def _delta_call_conversion_finished(self, user_data):
        # source_path, target_path = user_data
        self._post_conversion.resolve(*user_data)

    def __init__(self, parent):
        self._parent = parent
        self._pandoc = DeltaPandoc(self)
        self._post_conversion = DeltaPostConversion(self)
        DeltaSignalReceiver(self)
