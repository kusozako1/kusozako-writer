# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from lxml import etree
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity


class DeltaCss(DeltaEntity):

    def _get_head(self, root):
        for head in root.iter("head"):
            return head

    def resolve(self, root):
        head = self._get_head(root)
        css_element = etree.Element("style")
        css_element.attrib["type"] = "text/css"
        resource_directory = self._enquiry("delta > resource directory")
        names = [resource_directory, "css", "ocean.css"]
        path = GLib.build_filenamev(names)
        _, contents_bytes = GLib.file_get_contents(path)
        css_element.text = contents_bytes.decode("utf-8")
        head.insert(-1, css_element)

    def __init__(self, parent):
        self._parent = parent
