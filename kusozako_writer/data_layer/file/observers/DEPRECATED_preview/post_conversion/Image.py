# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity


class DeltaImage(DeltaEntity):

    def _is_absolute(self, uri):
        for key in ("/", "https://", "http://", "file://"):
            if uri.startswith(key):
                return True
        return False

    def _get_gfile(self, directory_gfile, image_uri):
        if self._is_absolute(image_uri):
            return Gio.File.parse_name(image_uri)
        else:
            return directory_gfile.get_child(image_uri)

    def resolve(self, root, source_path):
        source_gfile = Gio.File.new_for_path(source_path)
        directory_gfile = source_gfile.get_parent()
        for element in root.iter("img"):
            image_uri = element.attrib["src"]
            gfile = self._get_gfile(directory_gfile, image_uri)
            if not gfile.query_exists():
                continue
            _, contents_byte = GLib.file_get_contents(gfile.get_path())
            base64 = GLib.base64_encode(contents_byte)
            data = "data:image/jpeg;base64,{}".format(base64)
            element.attrib["src"] = data

    def __init__(self, parent):
        self._parent = parent
