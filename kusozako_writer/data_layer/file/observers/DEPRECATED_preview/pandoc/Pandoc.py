# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.Entity import DeltaEntity
from .WorkingDirectory import DeltaWorkingDirectory


class DeltaPandoc(DeltaEntity):

    def _finished(self, subprocess, task, source_path):
        success, _, _ = subprocess.communicate_finish(task)
        if success:
            user_data = source_path, self._working_directory.get_target_path()
            self._raise("delta > conversion finished", user_data)

    def create_preview(self, source_path):
        # DO NOT QUOTE paths
        command = [
            "pandoc",
            source_path,
            "--standalone",
            "--from", "markdown",
            "--to", "html5",
            "--output", self._working_directory.get_target_path()
            ]
        subprocess = Gio.Subprocess.new(command, Gio.SubprocessFlags.NONE)
        subprocess.communicate_async(None, None, self._finished, source_path)

    def __init__(self, parent):
        self._parent = parent
        self._working_directory = DeltaWorkingDirectory(self)
