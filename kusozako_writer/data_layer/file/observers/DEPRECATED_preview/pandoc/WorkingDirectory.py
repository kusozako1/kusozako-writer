# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity


class DeltaWorkingDirectory(DeltaEntity):

    def get_target_path(self):
        return self._gfile.get_path()

    def __init__(self, parent):
        self._parent = parent
        cache_directory = GLib.get_home_dir()
        application_id = self._enquiry("delta > data", "application-id")
        names = [cache_directory, application_id]
        directory_path = GLib.build_filenamev(names)
        directory_gfile = Gio.File.new_for_path(directory_path)
        if not directory_gfile.query_exists():
            directory_gfile.make_directory(None)
        self._gfile = directory_gfile.get_child("foobar.html")
