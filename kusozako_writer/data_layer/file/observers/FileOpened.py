# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from gi.repository import GtkSource
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from kusozako_writer.const import EditorSignals


class DeltaFileOpened(DeltaEntity):

    def _on_progress(self, current_bytes, total_bytes, user_data):
        print("progress", current_bytes/total_bytes)

    def _on_finished(self, loader, task, gfile):
        success = loader.load_finish(task)
        if success:
            user_data = EditorSignals.FILE_SAVED, gfile
            self._raise("delta > editor signal", user_data)

    def receive_transmission(self, user_data):
        signal, gfile = user_data
        if signal != MainWindowSignals.FILE_OPENED:
            return
        file_ = self._enquiry("delta > file")
        file_.set_location(gfile)
        buffer_ = self._enquiry("delta > buffer")
        loader = GtkSource.FileLoader(buffer=buffer_, file=file_)
        loader.load_async(
            GLib.PRIORITY_DEFAULT,      # prority
            None,                       # Cancellable
            self._on_progress,          # progress callback
            ("", ),                     # data for progress callback
            self._on_finished,          # finished callback
            gfile,                      # data for finish callback
            )

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register main window signal object", self)
