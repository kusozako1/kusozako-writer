# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals


class DeltaSignalReceiver(DeltaEntity):

    def _on_received(self):
        source_file = self._enquiry("delta > file")
        gfile = source_file.get_location()
        if gfile is None:
            self._raise("delta > request user selection")
        else:
            user_data = source_file, gfile
            self._raise("delta > save to", user_data)

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal != MainWindowSignals.TRY_SAVE_FILE:
            return
        self._on_received()

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register main window signal object", self)
