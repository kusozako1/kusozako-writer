# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GtkSource
from kusozako1.Entity import DeltaEntity
from .file_state.FileState import DeltaFileState
from .observers.Observers import EchoObservers


class DeltaFile(GtkSource.File, DeltaEntity):

    def _delta_info_file(self):
        return self

    def _delta_info_file_location(self):
        return self.get_location()

    def _delta_info_changes_not_saved(self):
        return self._file_state.get_changes_not_saved()

    def __init__(self, parent):
        self._parent = parent
        GtkSource.File.__init__(self)
        self._file_state = DeltaFileState(self)
        EchoObservers(self)
        self._raise("delta > loopback editor data layer ready", self)
