# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from gi.repository import GtkSource
from kusozako1.Entity import DeltaEntity
from kusozako_writer.const import EditorSignals

DEFAULT_FLAG = GtkSource.FileSaverFlags.NONE


class DeltaFileSaver(DeltaEntity):

    def _on_save_finished(self, file_saver, task, gfile):
        successed = file_saver.save_finish(task)
        if not successed:
            print("file is not saved ?")
        else:
            user_data = EditorSignals.FILE_SAVED, gfile
            self._raise("delta > editor signal", user_data)
            self._raise("delta > set new")

    def save_async(self, source_file, gfile, flags=DEFAULT_FLAG):
        buffer_ = self._enquiry("delta > buffer")
        file_saver = GtkSource.FileSaver.new_with_target(
            buffer_,
            source_file,
            gfile
            )
        file_saver.set_flags(flags)
        file_saver.save_async(
            GLib.PRIORITY_DEFAULT,          # async priority
            None,                           # cancellable
            None,                           # progress callback
            None,                           # progress callback data
            self._on_save_finished,         # callback
            gfile,                          # callback data
            )

    def __init__(self, parent):
        self._parent = parent
