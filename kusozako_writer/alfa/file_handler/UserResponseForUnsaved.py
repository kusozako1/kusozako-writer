# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals


class DeltaUserResponseUnsaved(DeltaEntity):

    def _discard(self):
        self._raise("delta > set new")

    def _save(self):
        source_file = self._enquiry("delta > file")
        gfile = source_file.get_location()
        if gfile is None:
            self._raise("delta > request user selection")
        else:
            user_data = source_file, gfile
            self._raise("delta > save to", user_data)

    def request(self):
        buttons = [
            (_("Cancel"), None),
            (_("Discard"), self._discard),
            (_("Save"), self._save)
            ]
        heading = _("Your Changes Not Saved")
        dialog_model = {
            "heading": heading,
            "buttons": buttons
            }
        user_data = MainWindowSignals.SHOW_DIALOG, dialog_model
        self._raise("delta > main window signal", user_data)

    def __init__(self, parent):
        self._parent = parent
