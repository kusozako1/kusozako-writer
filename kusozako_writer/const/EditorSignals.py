# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

FILE_SAVED = "file-saved"                           # gfile
INSERT_AT_CURSOR = "insert-at-cursor"               # markdown as str
REQUEST_RELATIVE_PATH = "request-relative-path"     # gfile for image
BUFFER_CHANGED = "buffer-changed"                   # str
CURSOR_MOVED = "cursor-moved"                       # line, line_count, offset
EXPORT_TO_PDF = "export-to-pdf"                     # gfile
EXPORT_TO_HTML = "export-to-html"                   # gfile
WEB_VIEW_SET_POSITION = "web-view-set-position"     # int as line number
WEB_VIEW_REQUSET_REFRESH = "web-view-request-refresh"   # markdown, line_number
