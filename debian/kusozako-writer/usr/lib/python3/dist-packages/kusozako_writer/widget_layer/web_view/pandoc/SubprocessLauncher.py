# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.Entity import DeltaEntity

FLAGS = Gio.SubprocessFlags.STDIN_PIPE | Gio.SubprocessFlags.STDOUT_PIPE


class DeltaSubprocessLauncher(DeltaEntity):

    def get_subprocess(self, command_line):
        subprocess_launcher = Gio.SubprocessLauncher.new(FLAGS)
        location = self._enquiry("delta > file location")
        if location is not None:
            parent_gfile = location.get_parent()
            subprocess_launcher.set_cwd(parent_gfile.get_path())
        subprocess = subprocess_launcher.spawnv(command_line)
        return subprocess

    def __init__(self, parent):
        self._parent = parent
