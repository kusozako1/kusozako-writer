# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.Entity import DeltaEntity


class DeltaMenuModel(Gio.Menu, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        Gio.Menu.__init__(self)
        self.append(_("Insert Image (Absolute)"), "edit.insert-image-absolute")
        self.append(_("Insert Image (Relative)"), "edit.insert-image-relative")
        view = self._enquiry("delta > view")
        view.set_extra_menu(self)
