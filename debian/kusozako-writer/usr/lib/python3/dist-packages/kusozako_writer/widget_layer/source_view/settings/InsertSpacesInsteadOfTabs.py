# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .SettingsEntity import AlfaSettingsEntity


class DeltaInsertSpacesInsteadOfTabs(AlfaSettingsEntity):

    KEY = "insert_spaces_instead_of_tabs"
    DEFAULT_VALUE = False
    PROPERTY_NAME = "insert-spaces-instead-of-tabs"
