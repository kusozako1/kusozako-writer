# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity


class DeltaTimeout(DeltaEntity):

    def _timeout(self, index, user_data):
        if self._index == index:
            self._raise("delta > timeout", user_data)
        return GLib.SOURCE_REMOVE

    def start_timeout(self, user_data):
        self._index += 1
        index = self._index
        GLib.timeout_add(100, self._timeout, index, user_data)

    def __init__(self, parent):
        self._parent = parent
        self._index = 0
