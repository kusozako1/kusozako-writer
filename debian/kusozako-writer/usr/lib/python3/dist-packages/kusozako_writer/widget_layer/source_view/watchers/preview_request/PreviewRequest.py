# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_writer.const import EditorSignals
from .Timeout import DeltaTimeout


class DeltaPreviewRequest(DeltaEntity):

    def _delta_call_timeout(self, buffer):
        if self._buffer_changed:
            markdown = buffer.get_text(
                buffer.get_start_iter(),
                buffer.get_end_iter(),
                False,
                )
            param = markdown, self._line
            user_data = EditorSignals.WEB_VIEW_REQUSET_REFRESH, param
            self._buffer_changed = False
        else:
            user_data = EditorSignals.WEB_VIEW_SET_POSITION, self._line
        self._raise("delta > editor signal", user_data)

    def _check_line_changed(self, buffer):
        iter_ = buffer.get_iter_at_offset(buffer.props.cursor_position)
        line = iter_.get_line()
        if line != self._line:
            self._timeout.start_timeout(buffer)
        self._line = line

    def _on_move_cursor(self, source_view, step, count, extend_selection):
        buffer = source_view.get_buffer()
        self._check_line_changed(buffer)

    def _on_changed(self, buffer):
        self._buffer_changed = True
        self._check_line_changed(buffer)

    def __init__(self, parent):
        self._parent = parent
        self._timeout = DeltaTimeout(self)
        self._line = 0
        self._buffer_changed = False
        view = self._enquiry("delta > view")
        view.connect_after("move-cursor", self._on_move_cursor)
        buffer = view.get_buffer()
        buffer.connect_after("changed", self._on_changed)
