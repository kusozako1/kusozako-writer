# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib

FROM = '<html xmlns="http://www.w3.org/1999/xhtml" lang xml:lang>'
FIXED = '<html xmlns="http://www.w3.org/1999/xhtml" lang="ja" xml:lang="ja">'


class FoxtrotCache:

    def save(self, html5):
        fixed_html = html5.replace(FROM, FIXED)
        GLib.file_set_contents(self._path, bytes(fixed_html, "utf-8"))

    def get_uri(self, id_=None):
        if id_ is None:
            return self._uri
        return "{}#{}".format(self._uri, id_)

    def __init__(self):
        gfile, _ = Gio.File.new_tmp("XXXXXXXX.html")
        self._path = gfile.get_path()
        self._uri = gfile.get_uri()
