# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later


class FoxtrotCodeBlockChecker:

    def _is_header(self, line):
        if not line.startswith("#"):
            return False
        if line.startswith("#######"):
            return False
        return True

    def is_heading(self, line):
        if line.startswith("```"):
            self._is_code_block = not self._is_code_block
        if self._is_code_block:
            return False
        return self._is_header(line)

    def reset(self):
        self._is_code_block = False

    def __init__(self):
        self._is_code_block = False
