# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GObject


class FoxtrotHeadingItem(GObject.Object):

    def get_line_index(self):
        return self._line_index

    def get_id(self):
        return self._id

    def __init__(self, line_index, id_):
        GObject.Object.__init__(self)
        self._line_index = line_index
        self._id = id_
