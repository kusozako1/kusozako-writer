# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .preview_request.PreviewRequest import DeltaPreviewRequest


class EchoWatchers:

    def __init__(self, parent):
        DeltaPreviewRequest(parent)
