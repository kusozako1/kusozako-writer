# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .SettingsEntity import AlfaSettingsEntity


class DeltaShowLineNumber(AlfaSettingsEntity):

    KEY = "show_line_number"
    DEFAULT_VALUE = False
    PROPERTY_NAME = "show-line-numbers"
