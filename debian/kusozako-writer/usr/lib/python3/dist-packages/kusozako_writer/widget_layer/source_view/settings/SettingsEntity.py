# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity


class AlfaSettingsEntity(DeltaEntity):

    GROUP = "view"
    KEY = "define key here"
    DEFAULT_VALUE = "define default value here"
    PROPERTY_NAME = "define property name here"

    def _refresh(self, new_value):
        view = self._enquiry("delta > view")
        view.set_property(self.PROPERTY_NAME, new_value)

    def receive_transmission(self, user_data):
        group, key, value = user_data
        if group != self.GROUP or key != self.KEY:
            return
        self._refresh(value)

    def __init__(self, parent):
        self._parent = parent
        query = self.GROUP, self.KEY, self.DEFAULT_VALUE
        startup_value = self._enquiry("delta > settings", query)
        self._refresh(startup_value)
        self._raise("delta > register settings object", self)
