# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .MenuModel import DeltaMenuModel
from .actions.Actions import EchoActions


class EchoExtraMenu:

    def __init__(self, parent):
        DeltaMenuModel(parent)
        EchoActions(parent)
