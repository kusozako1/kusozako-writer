# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import WebKit
from kusozako1.Entity import DeltaEntity
from .Cache import FoxtrotCache
from .pandoc.Pandoc import DeltaPandoc
from .heading_ids.HeadingIds import DeltaHeadingIds


class DeltaWebView(WebKit.WebView, DeltaEntity):

    def _delta_call_move_to_id(self, id_):
        if id_ == self._id:
            return
        self._id = id_
        uri = self._cache.get_uri(id_)
        self.load_uri(uri)

    def _delta_call_html5_created(self, html5):
        self._cache.save(html5)
        self.reload()

    def __init__(self, parent):
        self._parent = parent
        self._id = None
        self._cache = FoxtrotCache()
        DeltaPandoc(self)
        DeltaHeadingIds(self)
        WebKit.WebView.__init__(self, hexpand=True, vexpand=True)
        self.load_uri(self._cache.get_uri())
        self._raise("delta > add to container", self)
