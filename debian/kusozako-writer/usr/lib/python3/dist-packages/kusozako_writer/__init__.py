# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import gettext
import locale
import gi
from kusozako1.const import UserSpecialDirectories

gi.require_version('WebKit', '6.0')

VERSION = "2025.02.20"
APPLICATION_NAME = "kusozako-writer"
APPLICATION_ID = "com.gitlab.kusozako1.Writer"

locale.setlocale(locale.LC_ALL, None)
gettext.install(
    APPLICATION_NAME,
    "/usr/share/locale",
    names=('gettext', 'ngettext')
)

LONG_DESCRIPTION = """Markdown Editor for kusozako project.
This software is licensed under GPL v3+"""

APPLICATION_DATA = {
    "version": VERSION,
    "min-files": 0,
    "max-files": 1,
    "mime": ["text/markdown"],
    "default-file-directory": UserSpecialDirectories.HOME,
    "use-global-new": True,
    "use-global-open": True,
    "use-global-save": True,
    "use-global-save-as": True,
    "application-name": APPLICATION_NAME,
    "rdnn-name": APPLICATION_ID,
    "application-id": APPLICATION_ID,
    "long-description": LONG_DESCRIPTION
}
