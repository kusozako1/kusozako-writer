# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later


WRITER = "writer"
EXPORT = "export"
VIEW_CONFIG = "view-config"
STYLE_SCHEMES = "style-schemes"
FONT = "font"
FONT_FAMILY = "font-family"
FONT_SIZE = "font-size"
FONT_WEIGHT = "font-weight"
FONT_STYLE = "font-style"
