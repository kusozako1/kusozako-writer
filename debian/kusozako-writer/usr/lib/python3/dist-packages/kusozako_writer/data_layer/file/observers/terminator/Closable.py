# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals


class DeltaClosable(DeltaEntity):

    def _dispatch(self):
        if self._enquiry("delta > changes not saved"):
            self._raise("delta > request user response for unsaved")
        else:
            user_data = MainWindowSignals.SHOW_DIALOG, self._dialog_model
            self._raise("delta > main window signal", user_data)

    def force_close(self):
        self._is_closable = True
        user_data = MainWindowSignals.TRY_CLOSE, None
        self._raise("delta > main window signal", user_data)

    def request(self):
        if not self._is_closable:
            self._dispatch()
        return self._is_closable

    def __init__(self, parent):
        self._parent = parent
        self._is_closable = False
        buttons = [
            (_("Cancel"), None),
            (_("Close"), self.force_close)
            ]
        self._dialog_model = {
            "heading": _("Close Window ?"),
            "buttons": buttons
            }
