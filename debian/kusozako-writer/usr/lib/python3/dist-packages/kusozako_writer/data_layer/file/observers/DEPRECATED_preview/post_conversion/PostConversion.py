# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from lxml import etree
from kusozako1.Entity import DeltaEntity
from .Css import DeltaCss
from .Image import DeltaImage


class DeltaPostConversion(DeltaEntity):

    def resolve(self, source_path, target_path):
        parser = etree.HTMLParser()
        tree = etree.parse(target_path, parser=parser)
        root = tree.getroot()
        self._css.resolve(root)
        self._image.resolve(root, source_path)
        tree.write(target_path)

    def __init__(self, parent):
        self._parent = parent
        self._css = DeltaCss(self)
        self._image = DeltaImage(self)
