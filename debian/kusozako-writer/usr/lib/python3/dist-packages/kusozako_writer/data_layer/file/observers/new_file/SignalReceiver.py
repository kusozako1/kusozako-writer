# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals


class DeltaSignalReceiver(DeltaEntity):

    def _on_received(self):
        changes_not_saved = self._enquiry("delta > changes not saved")
        if changes_not_saved:
            self._raise("delta > request user response for unsaved")
        else:
            self._raise("delta > set new")

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal != MainWindowSignals.NEW_FILE:
            return
        self._on_received()

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register main window signal object", self)
