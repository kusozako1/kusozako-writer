# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_writer.alfa.file_handler.FileHandler import AlfaFileHandler
from .SignalReceiver import DeltaSignalReceiver


class DeltaTrySave(AlfaFileHandler):

    def _on_initialize(self):
        DeltaSignalReceiver(self)
