# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GtkSource
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals


class DeltaLanguageManager(GtkSource.LanguageManager, DeltaEntity):

    def receive_transmission(self, user_data):
        signal, gfile = user_data
        if signal != MainWindowSignals.FILE_OPENED:
            return
        path = gfile.get_path()
        file_info = gfile.query_info("*", 0)
        content_type = file_info.get_content_type()
        language = self.guess_language(path, content_type)
        self._raise("delta > language changed", language)

    def __init__(self, parent):
        self._parent = parent
        GtkSource.LanguageManager.__init__(self)
        self._raise("delta > register main window signal object", self)
