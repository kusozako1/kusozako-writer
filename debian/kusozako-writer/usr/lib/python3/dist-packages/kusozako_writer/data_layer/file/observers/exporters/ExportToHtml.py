# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from .Exporter import AlfaExporter


class DeltaExportToHtml(AlfaExporter):

    def _get_command_line(self, output_path):
        resource_directory = self._enquiry("delta > resource directory")
        names = [resource_directory, "css", "ocean.css"]
        css_path = GLib.build_filenamev(names)
        css_gfile = Gio.File.new_for_path(css_path)
        return [
            "pandoc",
            "-f", "markdown",
            "-t", "html",
            "--standalone",
            "--embed-resources",
            "--css", css_gfile.get_uri(),
            "-o", output_path,
            ]

    def _on_received(self, markdown, gfile):
        location = self._enquiry("delta > file location")
        if location is None:
            print("save file first")
        command_line = self._get_command_line(gfile.get_path())
        parent_gfile = location.get_parent()
        working_directory = parent_gfile.get_path()
        subprocess = self._get_subprocess(working_directory, command_line)
        subprocess.communicate_utf8_async(
            markdown,
            None,
            self._on_finished,
            gfile.get_path(),
            )
