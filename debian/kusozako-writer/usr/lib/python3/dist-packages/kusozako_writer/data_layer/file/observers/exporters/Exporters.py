# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .ExportToPdf import DeltaExportToPdf
from .ExportToHtml import DeltaExportToHtml


class EchoExporters:

    def __init__(self, parent):
        DeltaExportToPdf(parent)
        DeltaExportToHtml(parent)
