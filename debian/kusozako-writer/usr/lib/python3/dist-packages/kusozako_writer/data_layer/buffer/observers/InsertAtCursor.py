# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_writer.const import EditorSignals


class DeltaInsertAtCursor(DeltaEntity):

    def receive_transmission(self, user_data):
        signal, markdown = user_data
        if signal != EditorSignals.INSERT_AT_CURSOR:
            return
        buffer_ = self._enquiry("delta > buffer")
        buffer_.insert_at_cursor(markdown, -1)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register editor object", self)
