# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GtkSource
from kusozako1.Entity import DeltaEntity


class DeltaStyleSchemeManager(GtkSource.StyleSchemeManager, DeltaEntity):

    def _refresh(self, current_id):
        style_scheme = self.get_scheme(current_id)
        if style_scheme is not None:
            self._raise("delta > style scheme changed", style_scheme)

    def receive_transmission(self, user_data):
        group, key, id_ = user_data
        if group == "view" and key == "style_scheme":
            self._refresh(id_)

    def __init__(self, parent):
        self._parent = parent
        GtkSource.StyleSchemeManager.__init__(self)
        query = "view", "style_scheme", "Adwaita"
        id_ = self._enquiry("delta > settings", query)
        self._refresh(id_)
        self._raise("delta > register settings object", self)
