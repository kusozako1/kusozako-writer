# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from kusozako_writer.const import EditorSignals


class DeltaFileState(DeltaEntity):

    def _on_changed(self, buffer_):
        self._changes_not_saved = True

    def get_changes_not_saved(self):
        return self._changes_not_saved

    def receive_transmission(self, user_data):
        signal, gfile = user_data
        if signal != EditorSignals.FILE_SAVED:
            return
        self._changes_not_saved = False
        title = None if gfile is None else gfile.get_basename()
        user_data = MainWindowSignals.CHANGE_TITLE, title
        self._raise("delta > main window signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        self._changes_not_saved = False
        buffer_ = self._enquiry("delta > buffer")
        buffer_.connect("changed", self._on_changed)
        self._raise("delta > register editor object", self)
