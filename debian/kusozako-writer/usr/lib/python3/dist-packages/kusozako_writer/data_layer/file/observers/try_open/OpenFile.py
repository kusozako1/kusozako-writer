# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from kusozako1.file_dialog_2.FileDialog import DeltaFileDialog


class DeltaOpenFile(DeltaEntity):

    def _delta_call_dialog_response(self, gfile):
        user_data = MainWindowSignals.FILE_OPENED, gfile
        self._raise("delta > main window signal", user_data)

    def _get_gfile(self):
        directory = self._enquiry("delta > data", "default-file-directory")
        if directory is None:
            directory = GLib.get_home_dir()
        return Gio.File.new_for_path(directory)

    def open_file(self):
        gfile = self._get_gfile()
        DeltaFileDialog.select_file(
            self,
            mime_types=["text/markdown"],
            default_directory=gfile,
            )
        # filter_ = self._enquiry("delta > data", "mime")
        # DeltaFileDialog.open(self, gfile, filter_, "Open", "Open")

    def __init__(self, parent):
        self._parent = parent
