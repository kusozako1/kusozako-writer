# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_writer.alfa.file_handler.FileHandler import AlfaFileHandler
from .SignalReceiver import DeltaSignalReceiver
from .VoidFile import DeltaVoidFile


class DeltaNewFile(AlfaFileHandler):

    def _post_sequence(self):
        self._void_file.set_void()

    def _on_initialize(self):
        self._void_file = DeltaVoidFile(self)
        DeltaSignalReceiver(self)
