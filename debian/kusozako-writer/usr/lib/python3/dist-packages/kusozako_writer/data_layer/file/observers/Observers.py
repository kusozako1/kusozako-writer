# (c) copyright 2024-2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .new_file.NewFile import DeltaNewFile
from .try_open.TryOpen import DeltaTryOpen
from .FileOpened import DeltaFileOpened
from .try_save.TrySave import DeltaTrySave
from .try_save_as.TrySaveAs import DeltaTrySaveAs
from .terminator.Terminator import DeltaTerminator
from .RequestRelativePath import DeltaRequestRelativePath
from .exporters.Exporters import EchoExporters


class EchoObservers:

    def __init__(self, parent):
        DeltaNewFile(parent)
        DeltaTryOpen(parent)
        DeltaFileOpened(parent)
        DeltaTrySave(parent)
        DeltaTrySaveAs(parent)
        DeltaTerminator(parent)
        DeltaRequestRelativePath(parent)
        EchoExporters(parent)
