# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_writer.const import EditorSignals


class DeltaSignalReceiver(DeltaEntity):

    def receive_transmission(self, user_data):
        signal, gfile = user_data
        if signal != EditorSignals.FILE_SAVED:
            return
        self._raise("delta > create preview", gfile.get_path())

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register editor object", self)
