# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GtkSource
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals


class DeltaOverwriteConfirmation(DeltaEntity):

    def _close_dialog(self):
        user_data = MainWindowSignals.CLOSE_OVERLAY, None
        self._raise("delta > main window signal", user_data)

    def _create_backup(self, gfile):
        source_file = self._enquiry("delta > file")
        user_data = source_file, gfile, GtkSource.FileSaverFlags.CREATE_BACKUP
        self._raise("delta > save to", user_data)
        self._close_dialog()

    def _overwrite(self, gfile):
        source_file = self._enquiry("delta > file")
        user_data = source_file, gfile
        self._raise("delta > save to", user_data)
        self._close_dialog()

    def request(self, gfile):
        buttons = [
            (_("Cancel"), None),
            (_("Create Backup"), self._create_backup, gfile),
            (_("Overwrite"), self._overwrite, gfile)
            ]
        heading = "'{}' has been existed".format(gfile.get_basename())
        dialog_model = {
            "heading": heading,
            "buttons": buttons
            }
        user_data = MainWindowSignals.SHOW_DIALOG, dialog_model
        self._raise("delta > main window signal", user_data)

    def __init__(self, parent):
        self._parent = parent
