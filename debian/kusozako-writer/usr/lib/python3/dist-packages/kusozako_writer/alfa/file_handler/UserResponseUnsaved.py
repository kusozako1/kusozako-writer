# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals


class DeltaUserResponseUnsaved(DeltaEntity):

    def _close_dialog(self):
        user_data = MainWindowSignals.CLOSE_OVERLAY, None
        self._raise("delta > main window signal", user_data)

    def _save(self):
        self._close_dialog()
        source_file = self._enquiry("delta > file")
        gfile = source_file.get_location()
        if gfile is not None:
            user_data = source_file, gfile
            self._raise("delta > save to", user_data)
        else:
            self._raise("delta > request user selection")

    def _discard(self):
        self._close_dialog()
        self._raise("delta > set new")

    def request(self):
        buttons = [
            (_("Cancel"), None),
            (_("Save"), self._save),
            (_("Discard"), self._discard)
            ]
        dialog_model = {
            "heading": "Changes not Saved.",
            "buttons": buttons
            }
        user_data = MainWindowSignals.SHOW_DIALOG, dialog_model
        self._raise("delta > main window signal", user_data)

    def __init__(self, parent):
        self._parent = parent
