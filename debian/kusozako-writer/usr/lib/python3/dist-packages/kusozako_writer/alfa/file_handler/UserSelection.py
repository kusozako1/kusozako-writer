# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.Entity import DeltaEntity
from kusozako1.util import SafePath
from kusozako1.file_dialog_2.FileDialog import DeltaFileDialog


class DeltaUserSelection(DeltaEntity):

    def _delta_call_dialog_response(self, gfile):
        if gfile.query_exists():
            self._raise("delta > request overwrite confirmation", gfile)
        else:
            source_file = self._enquiry("delta > file")
            user_data = source_file, gfile
            self._raise("delta > save to", user_data)

    def _get_directory_gfile(self):
        path = self._enquiry("delta > data", "default-file-directory")
        return Gio.File.new_for_path(path)

    def request(self):
        gfile = self._get_directory_gfile()
        filename = SafePath.get_safe_name(gfile, "foobar.md")
        DeltaFileDialog.save_file(
            self,
            title=_("Save file to..."),
            mime_types=["text/markdown"],
            default_filename=filename,
            default_directory=gfile,
            )

    def __init__(self, parent):
        self._parent = parent
