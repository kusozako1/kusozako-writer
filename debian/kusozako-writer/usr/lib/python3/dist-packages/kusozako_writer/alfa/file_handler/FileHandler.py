# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from .UserResponseForUnsaved import DeltaUserResponseUnsaved
from .FileSaver import DeltaFileSaver
from .UserSelection import DeltaUserSelection
from .OverwriteConfirmation import DeltaOverwriteConfirmation


class AlfaFileHandler(DeltaEntity):

    def _close_overlay(self):
        user_data = MainWindowSignals.CLOSE_OVERLAY, None
        self._raise("delta > main window signal", user_data)

    def _post_sequence(self):
        pass

    def _delta_call_set_new(self):
        self._close_overlay()
        self._post_sequence()

    def _delta_call_request_user_response_for_unsaved(self):
        self._user_response_unsaved.request()

    def _delta_call_save_to(self, user_data):
        self._file_saver.save_async(*user_data)

    def _delta_call_request_user_selection(self):
        self._user_selection.request()

    def _delta_call_request_overwrite_confirmation(self, gfile):
        self._overwrite_confirmation.request(gfile)

    def _on_initialize(self):
        pass

    def __init__(self, parent):
        self._parent = parent
        self._user_response_unsaved = DeltaUserResponseUnsaved(self)
        self._file_saver = DeltaFileSaver(self)
        self._user_selection = DeltaUserSelection(self)
        self._overwrite_confirmation = DeltaOverwriteConfirmation(self)
        self._on_initialize()
