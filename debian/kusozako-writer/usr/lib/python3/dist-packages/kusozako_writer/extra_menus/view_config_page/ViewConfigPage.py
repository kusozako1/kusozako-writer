# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.sub_page.SubPage import AlfaSubPage
from kusozako1.overlay_item.Label import DeltaLabel
from kusozako_writer.const import ExtraMenuPages
from .HighlightCurrentLineButton import DeltaHighlightCurrentLineButton
from .ShowGridButton import DeltaShowGridButton
from .ShowLineNumberButton import DeltaShowLineNumberButton
from .InsertSpaceInsteadOfTabButton import DeltaInsertSpaceInsteadOfTabButton
from .Size2 import DeltaSize2
from .Size4 import DeltaSize4
from .Size8 import DeltaSize8


class DeltaViewConfigPage(AlfaSubPage):

    PAGE_NAME = ExtraMenuPages.VIEW_CONFIG
    BACK_TO = ExtraMenuPages.WRITER

    def _on_initialize(self):
        DeltaShowGridButton(self)
        DeltaHighlightCurrentLineButton(self)
        DeltaShowLineNumberButton(self)
        DeltaInsertSpaceInsteadOfTabButton(self)
        DeltaLabel.new_for_label(self, _("Tab Size"))
        DeltaSize2(self)
        DeltaSize4(self)
        DeltaSize8(self)
