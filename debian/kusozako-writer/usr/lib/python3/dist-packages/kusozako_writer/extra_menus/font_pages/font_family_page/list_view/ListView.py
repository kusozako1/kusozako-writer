# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaListView(Gtk.ListView, DeltaEntity):

    def _on_setup(self, factory, list_item):
        label = Gtk.Label(xalign=0)
        list_item.set_child(label)

    def _on_bind(self, factory, list_item):
        label = list_item.get_child()
        font_entity = list_item.get_item()
        font_family = font_entity.get_string()
        label.set_label(font_family)

    def _on_activate(self, list_view, index):
        model = list_view.get_model()
        font_entity = model[index]
        settings = "css", "editor_font_family", font_entity.get_string()
        self._raise("delta > settings", settings)

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow(hexpand=True, vexpand=True)
        factory = Gtk.SignalListItemFactory()
        factory.connect("setup", self._on_setup)
        factory.connect("bind", self._on_bind)
        Gtk.ListView.__init__(
            self,
            opacity=0.9,
            factory=factory,
            model=self._enquiry("delta > model"),
            single_click_activate=True,
            )
        self.connect("activate", self._on_activate)
        self.add_css_class("kusozako-osd")
        scrolled_window.set_child(self)
        self._raise("delta > add to container", scrolled_window)
