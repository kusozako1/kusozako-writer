# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .base_model.BaseModel import DeltaBaseModel


class DeltaModels(DeltaEntity):

    def get_selection_model(self):
        return self._selection_model

    def __init__(self, parent):
        self._parent = parent
        base_model = DeltaBaseModel(self)
        self._selection_model = Gtk.MultiSelection.new(base_model)
