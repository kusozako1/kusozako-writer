# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .FontWeightButton import AlfaFontWeightButton


class DeltaBlackButton(AlfaFontWeightButton):

    LABEL = _("Black (Heavy)")
    MATCH_VALUE = 900
