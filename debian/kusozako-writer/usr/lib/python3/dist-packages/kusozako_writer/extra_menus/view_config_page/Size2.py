# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.SettingsButton import AlfaSettingsButton


class DeltaSize2(AlfaSettingsButton):

    LABEL = "2"
    GROUP = "view"
    KEY = "tab_width"
    MATCH_VALUE = 2
