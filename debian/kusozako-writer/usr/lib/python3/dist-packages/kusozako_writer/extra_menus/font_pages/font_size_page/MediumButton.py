# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .FontSizeButton import AlfaFontSizeButton


class DeltaMediumButton(AlfaFontSizeButton):

    LABEL = _("Medium")
    MATCH_VALUE = "medium"
