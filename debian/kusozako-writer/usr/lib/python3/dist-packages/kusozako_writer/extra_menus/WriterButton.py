# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton
from kusozako1.const import MainWindowSignals as Signals
from kusozako_writer.const import ExtraMenuPages


class DeltaWriterButton(AlfaButton):

    START_ICON = "text-editor-symbolic"
    LABEL = _("Writer")
    END_ICON = "go-next-symbolic"

    def _on_clicked(self, button):
        user_data = Signals.SHOW_EXTRA_PRIMARY_MENU, ExtraMenuPages.WRITER
        self._raise("delta > main window signal", user_data)

    def _set_to_container(self):
        user_data = Signals.ADD_EXTRA_MENU, self
        self._raise("delta > main window signal", user_data)
