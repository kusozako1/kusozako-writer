# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .FontSizeButton import AlfaFontSizeButton


class DeltaExtraExtraSmallButton(AlfaFontSizeButton):

    LABEL = _("Extra Extra Small")
    MATCH_VALUE = "xx-small"
