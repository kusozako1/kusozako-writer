# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .FontWeightButton import AlfaFontWeightButton


class DeltaBoldButton(AlfaFontWeightButton):

    LABEL = _("Bold")
    MATCH_VALUE = 700
