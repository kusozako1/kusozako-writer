# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .WriterButton import DeltaWriterButton
from .ExportButton import DeltaExportButton
from .writer_page.WriterPage import DeltaWriterPage
from .export_page.ExportPage import DeltaExportPage
from .view_config_page.ViewConfigPage import DeltaViewConfigPage
from .style_scheme_page.StyleSchemePage import DeltaStyleSchemePage
from .font_pages.FontPages import EchoFontPages


class EchoExtraMenus:

    def __init__(self, parent):
        DeltaWriterButton(parent)
        DeltaExportButton(parent)
        DeltaWriterPage(parent)
        DeltaExportPage(parent)
        DeltaViewConfigPage(parent)
        DeltaStyleSchemePage(parent)
        EchoFontPages(parent)
