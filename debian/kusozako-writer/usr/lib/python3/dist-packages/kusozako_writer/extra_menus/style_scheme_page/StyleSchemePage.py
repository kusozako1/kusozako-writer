# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GtkSource
from kusozako1.overlay_item.sub_page.SubPage import AlfaSubPage
from kusozako_writer.const import ExtraMenuPages
from .button.Button import DeltaButton


class DeltaStyleSchemePage(AlfaSubPage):

    PAGE_NAME = ExtraMenuPages.STYLE_SCHEMES
    BACK_TO = ExtraMenuPages.WRITER

    def _on_initialize(self):
        style_scheme_manager = GtkSource.StyleSchemeManager.get_default()
        for id_ in style_scheme_manager.get_scheme_ids():
            DeltaButton.new(self, id_)
