# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.Entity import DeltaEntity


class DeltaBuilder(DeltaEntity):

    def _parse(self, stdout):
        for line in stdout.split("\n"):
            if not line:
                break
            self._raise("delta > data found", line)

    def _on_commumicate_finished(self, subprocess, task, user_data=None):
        success, stdout, _ = subprocess.communicate_utf8_finish(task)
        if success:
            self._parse(stdout)

    def __init__(self, parent):
        self._parent = parent
        subprocess = Gio.Subprocess.new(
            ["fc-list"],
            Gio.SubprocessFlags.STDOUT_PIPE
            )
        subprocess.communicate_utf8_async(
            None,                           # stdin buffer
            None,                           # Cancellable
            self._on_commumicate_finished,  # callback
            None,                           # data for callback
            )
