# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton
from kusozako1.const import MainWindowSignals as Signals
from kusozako_writer.const import ExtraMenuPages


class DeltaFontFamilyButton(AlfaButton):

    LABEL = _("Font Family")
    END_ICON = "go-next-symbolic"

    def _on_clicked(self, button):
        user_data = Signals.SHOW_EXTRA_PRIMARY_MENU, ExtraMenuPages.FONT_FAMILY
        self._raise("delta > main window signal", user_data)
