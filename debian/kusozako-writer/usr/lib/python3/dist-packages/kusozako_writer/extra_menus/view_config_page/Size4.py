# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.SettingsButton import AlfaSettingsButton


class DeltaSize4(AlfaSettingsButton):

    LABEL = "4"
    GROUP = "view"
    KEY = "tab_width"
    MATCH_VALUE = 4
