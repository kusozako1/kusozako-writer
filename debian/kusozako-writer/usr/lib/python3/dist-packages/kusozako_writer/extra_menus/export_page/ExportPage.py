# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.sub_page.SubPage import AlfaSubPage
from kusozako_writer.const import ExtraMenuPages
from .Pdf import DeltaPdf


class DeltaExportPage(AlfaSubPage):

    PAGE_NAME = ExtraMenuPages.EXPORT

    def _on_initialize(self):
        DeltaPdf(self)
