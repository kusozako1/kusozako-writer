# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .font_page.FontPage import DeltaFontPage
from .font_family_page.FontFamilyPage import DeltaFontFamilyPage
from .font_size_page.FontSizePage import DeltaFontSizePage
from .font_weight_page.FontWeightPage import DeltaFontWeightPage


class EchoFontPages:

    def __init__(self, parent):
        DeltaFontPage(parent)
        DeltaFontFamilyPage(parent)
        DeltaFontSizePage(parent)
        DeltaFontWeightPage(parent)
