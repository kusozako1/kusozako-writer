# kusozako-writer

markdown editor for linux

## todo

- [ ] recent paths
- [ ] open file by command line
- [ ] source format selection
- [ ] css selection for prreview
- [ ] export PDF
- [ ] export standalone HTML5
- [ ] export ePub
- [ ] presentation

## License

This software is licensed under GPLv3 or any later version.
